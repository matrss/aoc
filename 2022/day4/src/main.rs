use std::fs;
use std::io::{self, prelude::*};

struct Range {
    min: u32,
    max: u32,
}

impl Range {
    fn from_assignment(s: &str) -> Self {
        let range = s
            .split("-")
            .map(|x| x.parse::<u32>().unwrap())
            .collect::<Vec<_>>();
        Range {
            min: range[0],
            max: range[1],
        }
    }

    fn contained_in(&self, rhs: &Range) -> bool {
        self.min >= rhs.min && self.max <= rhs.max
    }

    fn overlaps_with(&self, rhs: &Range) -> bool {
        (self.min <= rhs.max && self.max >= rhs.min) || (self.max >= rhs.min && self.min <= rhs.max)
    }
}

fn main() {
    let file = fs::File::open("input.txt").unwrap();
    let reader = io::BufReader::new(file);

    let mut total_fully_contained_ranges = 0;
    let mut total_overlapping_ranges = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let assignments = line.split(",").collect::<Vec<_>>();
        let range1 = Range::from_assignment(&assignments[0]);
        let range2 = Range::from_assignment(&assignments[1]);
        if range1.contained_in(&range2) || range2.contained_in(&range1) {
            total_fully_contained_ranges = total_fully_contained_ranges + 1;
        }
        if range1.overlaps_with(&range2) {
            total_overlapping_ranges = total_overlapping_ranges + 1;
        }
    }

    println!("part1: {}", total_fully_contained_ranges);
    println!("part2: {}", total_overlapping_ranges);
}
