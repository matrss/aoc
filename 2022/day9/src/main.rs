use std::collections::HashSet;
use std::fs;
use std::io::{self, prelude::*};
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Clone, Copy, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug)]
enum ParseDirectionError {
    InvalidToken,
}

impl FromStr for Direction {
    type Err = ParseDirectionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Direction::*;
        match s {
            "U" => Ok(Up),
            "D" => Ok(Down),
            "L" => Ok(Left),
            "R" => Ok(Right),
            _ => Err(Self::Err::InvalidToken),
        }
    }
}

#[derive(Eq, PartialEq, Hash, Clone, Copy, Debug)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    fn neighbor(self, direction: Direction) -> Self {
        use Direction::*;
        match direction {
            Up => Self::new(self.x, self.y + 1),
            Down => Self::new(self.x, self.y - 1),
            Left => Self::new(self.x - 1, self.y),
            Right => Self::new(self.x + 1, self.y),
        }
    }

    fn is_neighbor_of(&self, pos: Position) -> bool {
        self.x >= pos.x - 1 && self.x <= pos.x + 1 && self.y >= pos.y - 1 && self.y <= pos.y + 1
    }
}

#[derive(Debug)]
struct Rope {
    rope: [Position; 10],
}

impl Rope {
    fn new() -> Self {
        let rope = [Position::new(0, 0); 10];
        Self { rope }
    }

    fn step(self, direction: Direction) -> Self {
        let mut new_rope = self.rope;
        new_rope[0] = self.rope[0].neighbor(direction);
        for i in 1..10 {
            new_rope[i] = if new_rope[i].is_neighbor_of(new_rope[i - 1]) {
                new_rope[i]
            } else {
                let Position { x, y } = new_rope[i];
                if x == new_rope[i - 1].x && y < new_rope[i - 1].y {
                    Position::new(x, y + 1)
                } else if x == new_rope[i - 1].x && y > new_rope[i - 1].y {
                    Position::new(x, y - 1)
                } else if x < new_rope[i - 1].x && y == new_rope[i - 1].y {
                    Position::new(x + 1, y)
                } else if x > new_rope[i - 1].x && y == new_rope[i - 1].y {
                    Position::new(x - 1, y)
                } else if x < new_rope[i - 1].x && y < new_rope[i - 1].y {
                    Position::new(x + 1, y + 1)
                } else if x < new_rope[i - 1].x && y > new_rope[i - 1].y {
                    Position::new(x + 1, y - 1)
                } else if x > new_rope[i - 1].x && y < new_rope[i - 1].y {
                    Position::new(x - 1, y + 1)
                } else if x > new_rope[i - 1].x && y > new_rope[i - 1].y {
                    Position::new(x - 1, y - 1)
                } else {
                    unreachable!();
                }
            };
        }
        Self { rope: new_rope }
    }
}

struct Action {
    direction: Direction,
    num_steps: usize,
}

#[derive(Debug)]
enum ParseActionError {
    InvalidData,
    ParseDirection(ParseDirectionError),
    ParseInt(ParseIntError),
}

impl From<ParseDirectionError> for ParseActionError {
    fn from(f: ParseDirectionError) -> Self {
        Self::ParseDirection(f)
    }
}

impl From<ParseIntError> for ParseActionError {
    fn from(f: ParseIntError) -> Self {
        Self::ParseInt(f)
    }
}

impl FromStr for Action {
    type Err = ParseActionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (direction, num_steps) = s.split_once(' ').ok_or(Self::Err::InvalidData)?;
        let direction = direction.parse::<Direction>()?;
        let num_steps = num_steps.parse::<usize>()?;
        Ok(Self {
            direction,
            num_steps,
        })
    }
}

#[derive(Debug)]
enum ProgramError {
    IO(io::Error),
    ParseAction(ParseActionError),
}

impl From<io::Error> for ProgramError {
    fn from(f: io::Error) -> Self {
        ProgramError::IO(f)
    }
}

impl From<ParseActionError> for ProgramError {
    fn from(f: ParseActionError) -> Self {
        ProgramError::ParseAction(f)
    }
}

fn main() -> Result<(), ProgramError> {
    let file = fs::File::open("input.txt")?;
    let reader = io::BufReader::new(file);

    let mut rope = Rope::new();
    let mut visited_part1: HashSet<Position> = HashSet::new();
    let mut visited_part2: HashSet<Position> = HashSet::new();
    visited_part1.insert(rope.rope[1]);
    visited_part2.insert(rope.rope[9]);

    for line in reader.lines() {
        let line = line?;
        let action = line.parse::<Action>()?;

        for _ in 0..action.num_steps {
            rope = rope.step(action.direction);
            visited_part1.insert(rope.rope[1]);
            visited_part2.insert(rope.rope[9]);
        }
    }

    println!("part1: {}", visited_part1.len());
    println!("part2: {}", visited_part2.len());

    Ok(())
}
