use std::collections::HashSet;
use std::fs;
use std::io;

fn unique_chars_substrings(s: &str, len: usize) -> Vec<usize> {
    let mut unique_chars = vec![];
    for i in 0..(s.len() - len) {
        unique_chars.push(s[i..(i + len)].chars().collect::<HashSet<_>>().len());
    }
    unique_chars
}

fn main() -> io::Result<()> {
    let input_path = "input.txt";
    let content = fs::read_to_string(input_path)?;
    let sub_str_len = 4;

    let first_pos = unique_chars_substrings(&content, sub_str_len)
        .iter()
        .position(|&x| x == sub_str_len)
        .map(|x| x + sub_str_len);

    if let Some(pos) = first_pos {
        println!("part1: {}", pos);
    } else {
        println!(
            "No substring with unique letters of len {} found",
            sub_str_len
        );
    }

    let sub_str_len = 14;

    let first_pos = unique_chars_substrings(&content, sub_str_len)
        .iter()
        .position(|&x| x == sub_str_len)
        .map(|x| x + sub_str_len);

    if let Some(pos) = first_pos {
        println!("part2: {}", pos);
    } else {
        println!(
            "No substring with unique letters of len {} found",
            sub_str_len
        );
    }

    Ok(())
}
