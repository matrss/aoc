use std::fs;
use std::io::{self, prelude::*};

enum RPSMoves {
    Rock,
    Paper,
    Scissors,
}

fn str_to_move(s: &str) -> RPSMoves {
    match s {
        "A" => RPSMoves::Rock,
        "B" => RPSMoves::Paper,
        "C" => RPSMoves::Scissors,
        "X" => RPSMoves::Rock,
        "Y" => RPSMoves::Paper,
        "Z" => RPSMoves::Scissors,
        _ => panic!(),
    }
}

enum RPSResult {
    P1Win,
    P2Win,
    Draw,
}

fn str_to_result(s: &str) -> RPSResult {
    use RPSResult::*;
    match s {
        "X" => P1Win,
        "Y" => Draw,
        "Z" => P2Win,
        _ => panic!(),
    }
}

fn play(p1: &RPSMoves, p2: &RPSMoves) -> RPSResult {
    use RPSMoves::*;
    use RPSResult::*;
    match (p1, p2) {
        (Rock, Rock) => Draw,
        (Rock, Paper) => P2Win,
        (Rock, Scissors) => P1Win,
        (Paper, Paper) => Draw,
        (Paper, Rock) => P1Win,
        (Paper, Scissors) => P2Win,
        (Scissors, Scissors) => Draw,
        (Scissors, Rock) => P2Win,
        (Scissors, Paper) => P1Win,
    }
}

fn move_to_score(s: &RPSMoves) -> i32 {
    use RPSMoves::*;
    match s {
        Rock => 1,
        Paper => 2,
        Scissors => 3,
    }
}

fn result_to_score(s: &RPSResult) -> (i32, i32) {
    use RPSResult::*;
    match s {
        P1Win => (6, 0),
        P2Win => (0, 6),
        Draw => (3, 3),
    }
}

fn result_to_move(wanted_result: &RPSResult, opponent_move: &RPSMoves) -> RPSMoves {
    use RPSMoves::*;
    use RPSResult::*;
    match (wanted_result, opponent_move) {
        (P1Win, Rock) => Scissors,
        (P1Win, Paper) => Rock,
        (P1Win, Scissors) => Paper,
        (P2Win, Rock) => Paper,
        (P2Win, Paper) => Scissors,
        (P2Win, Scissors) => Rock,
        (Draw, Rock) => Rock,
        (Draw, Paper) => Paper,
        (Draw, Scissors) => Scissors,
    }
}

fn main() {
    let file = fs::File::open("input.txt").unwrap();
    let reader = io::BufReader::new(file);

    let mut total_points = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let mut moves = line.split(" ");
        let opponent_move = str_to_move(moves.next().unwrap());
        let my_move = str_to_move(moves.next().unwrap());
        let result = play(&opponent_move, &my_move);
        let score = move_to_score(&my_move) + result_to_score(&result).1;
        total_points = total_points + score;
    }

    println!("total points part1: {}", total_points);

    let file = fs::File::open("input.txt").unwrap();
    let reader = io::BufReader::new(file);

    let mut total_points = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let mut moves = line.split(" ");
        let opponent_move = str_to_move(moves.next().unwrap());
        let wanted_result = str_to_result(moves.next().unwrap());
        let my_move = result_to_move(&wanted_result, &opponent_move);
        let result = play(&opponent_move, &my_move);
        let score = move_to_score(&my_move) + result_to_score(&result).1;
        total_points = total_points + score;
    }

    println!("total points part2: {}", total_points);
}
