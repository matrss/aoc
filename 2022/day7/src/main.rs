use derive_more::{IsVariant, Unwrap};
use std::fs;
use std::io::{self, prelude::*};
use thiserror::Error;

#[derive(Debug)]
struct FSDirectory {
    name: String,
    elements: Vec<FSElem>,
}

impl FSDirectory {
    fn new(name: &str) -> Self {
        let name = name.to_owned();
        FSDirectory {
            name,
            elements: Vec::new(),
        }
    }

    fn insert(&mut self, elem: FSElem) {
        self.elements.push(elem);
    }

    fn get(&self, name: &str) -> Option<&FSElem> {
        self.elements.iter().find(|x| match x {
            FSElem::Directory(x) => x.name.as_str() == name,
            FSElem::File(x) => x.name.as_str() == name,
        })
    }

    fn get_mut(&mut self, name: &str) -> Option<&mut FSElem> {
        self.elements.iter_mut().find(|x| match x {
            FSElem::Directory(x) => x.name.as_str() == name,
            FSElem::File(x) => x.name.as_str() == name,
        })
    }

    fn size(&self) -> usize {
        self.elements
            .iter()
            .map(|x| match x {
                FSElem::File(x) => x.size,
                FSElem::Directory(x) => x.size(),
            })
            .sum()
    }
}

#[derive(Debug)]
struct FSFile {
    name: String,
    size: usize,
}

impl FSFile {
    fn new(name: String, size: usize) -> Self {
        Self { name, size }
    }
}

#[derive(IsVariant, Unwrap, Debug)]
enum FSElem {
    Directory(FSDirectory),
    File(FSFile),
}

#[derive(Error, Debug)]
enum FSError {
    #[error("cannot create filesystem elements in a file")]
    ParentIsFile,
}

#[derive(Debug)]
struct FS {
    root: FSDirectory,
}

impl FS {
    fn new() -> Self {
        let root = FSDirectory::new("");
        FS { root }
    }

    fn insert(&mut self, path: &[&str], elem: FSElem) -> Result<(), FSError> {
        let mut current_dir: &mut FSDirectory = &mut self.root;
        for part in &path[1..] {
            if let Some(x) = current_dir.get(part) {
                match x {
                    FSElem::Directory(_) => {
                        if let FSElem::Directory(x) = current_dir.get_mut(part).unwrap() {
                            current_dir = x;
                        } else {
                            unreachable!()
                        }
                    }
                    FSElem::File(_) => Err(FSError::ParentIsFile)?,
                }
            } else {
                let x = FSDirectory::new(part);
                let name = x.name.clone();
                current_dir.insert(FSElem::Directory(x));
                if let FSElem::Directory(x) = current_dir.get_mut(&name).unwrap() {
                    current_dir = x;
                } else {
                    unreachable!()
                }
            }
        }
        current_dir.insert(elem);
        Ok(())
    }
}

#[derive(Error, Debug)]
enum ProgramError {
    #[error("invalid data")]
    InvalidData,
}

fn all_directories(directory: &FSDirectory) -> Vec<&FSDirectory> {
    let mut directories = directory
        .elements
        .iter()
        .filter_map(|x| match x {
            FSElem::Directory(x) => Some(x),
            _ => None,
        })
        .collect::<Vec<_>>();
    for sub_dir in directory.elements.iter().filter_map(|x| match x {
        FSElem::Directory(x) => Some(x),
        _ => None,
    }) {
        let sub_dirs = all_directories(sub_dir);
        directories.extend(sub_dirs);
    }
    directories
}

fn fs_from_shell_session<T: BufRead>(reader: T) -> anyhow::Result<FS> {
    let mut fs = FS::new();
    let mut cwd: Vec<String> = vec!["".into()];
    for line in reader.lines() {
        let line = line?;
        if let Some(x) = line.strip_prefix("$ cd ") {
            match x {
                "/" => cwd = vec!["".into()],
                ".." => {
                    cwd.pop();
                }
                x => cwd.push(x.into()),
            }
        } else if let Some(x) = line.strip_prefix("dir ") {
            let elem = FSElem::Directory(FSDirectory::new(x));
            fs.insert(&cwd.iter().map(|x| x.as_str()).collect::<Vec<_>>(), elem)?;
        } else if line != "$ ls" {
            let (size, file_name) = line.split_once(' ').ok_or(ProgramError::InvalidData)?;
            let size = size.parse::<usize>()?;
            let elem = FSElem::File(FSFile::new(file_name.to_owned(), size));
            fs.insert(&cwd.iter().map(|x| x.as_str()).collect::<Vec<_>>(), elem)?;
        }
    }
    Ok(fs)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_test_input() {
        let file = fs::File::open("test_input.txt").unwrap();
        let reader = io::BufReader::new(file);
        let fs = fs_from_shell_session(reader).unwrap();
        let all_directories = all_directories(&fs.root);
        let total_size = all_directories
            .iter()
            .map(|x| x.size())
            .filter(|&x| x <= 100000)
            .sum::<usize>();
        assert_eq!(total_size, 95437);
    }
}

fn main() -> anyhow::Result<()> {
    let file = fs::File::open("input.txt")?;
    let reader = io::BufReader::new(file);

    let fs = fs_from_shell_session(reader)?;
    let all_directories = all_directories(&fs.root);
    let total_size = all_directories
        .iter()
        .map(|x| x.size())
        .filter(|&x| x <= 100000)
        .sum::<usize>();
    println!("part1: {}", total_size);

    let total_disk_space = 70000000;
    let required_disk_space = 30000000;
    let missing_free_space = required_disk_space - (total_disk_space - fs.root.size());
    let dir_to_delete_size = all_directories
        .iter()
        .map(|x| x.size())
        .filter(|&x| x >= missing_free_space)
        .min()
        .expect("no directory large enough");
    println!("part2: {}", dir_to_delete_size);

    Ok(())
}
