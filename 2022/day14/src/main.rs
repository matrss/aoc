use derive_more::IsVariant;
use std::collections::HashMap;
use std::fs;
use std::io::{self, prelude::*};
use std::num::ParseIntError;
use thiserror::Error;

#[derive(IsVariant, Clone, Copy, Debug)]
enum Tile {
    Air,
    Sand,
    Rock,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
struct Point {
    x: usize,
    y: usize,
}

impl From<(usize, usize)> for Point {
    fn from(t: (usize, usize)) -> Self {
        Point { x: t.0, y: t.1 }
    }
}

#[derive(Clone, Copy, Debug)]
struct Extend {
    min_x: usize,
    min_y: usize,
    max_x: usize,
    max_y: usize,
}

impl Extend {
    fn update(&mut self, k: Point) {
        if k.x < self.min_x {
            self.min_x = k.x;
        }
        if k.y < self.min_y {
            self.min_y = k.y;
        }
        if k.x > self.max_x {
            self.max_x = k.x;
        }
        if k.y > self.max_y {
            self.max_y = k.y;
        }
    }
}

#[derive(Debug)]
struct Cave {
    tiles: HashMap<Point, Tile>,
    extend: Extend,
    floor_level: Option<usize>,
}

#[derive(Error, Debug)]
enum ParseCaveError {}

#[derive(Error, Debug)]
enum CaveError {
    #[error("failed to parse an integer")]
    ParseInt(#[from] ParseIntError),
}

enum DropOutcome {
    Settled,
    Falloff,
    Plugged,
}

impl Cave {
    fn new() -> Self {
        Self {
            tiles: HashMap::new(),
            extend: Extend {
                min_x: usize::max_value(),
                min_y: usize::max_value(),
                max_x: usize::min_value(),
                max_y: usize::min_value(),
            },
            floor_level: None,
        }
    }

    fn set_tile(&mut self, k: Point, v: Tile) {
        self.tiles.insert(k, v);
        self.extend.update(k);
    }

    fn get(&self, k: &Point) -> Option<&Tile> {
        if let Some(floor_level) = self.floor_level {
            if k.y == floor_level {
                return Some(&Tile::Rock);
            }
        }
        self.tiles.get(k)
    }

    fn drop_sand(&mut self, start: Point) -> DropOutcome {
        let mut pos = start;
        loop {
            if pos.y > self.extend.max_y {
                return DropOutcome::Falloff;
            }
            if self
                .get(&Point {
                    y: pos.y + 1,
                    ..pos
                })
                .is_none()
            {
                pos = Point {
                    y: pos.y + 1,
                    ..pos
                };
                continue;
            }
            if self
                .get(&Point {
                    x: pos.x - 1,
                    y: pos.y + 1,
                })
                .is_none()
            {
                pos = Point {
                    x: pos.x - 1,
                    y: pos.y + 1,
                };
                continue;
            }
            if self
                .get(&Point {
                    x: pos.x + 1,
                    y: pos.y + 1,
                })
                .is_none()
            {
                pos = Point {
                    x: pos.x + 1,
                    y: pos.y + 1,
                };
                continue;
            }
            self.set_tile(pos, Tile::Sand);
            if pos == (Point { x: 500, y: 0 }) {
                return DropOutcome::Plugged;
            }
            return DropOutcome::Settled;
        }
    }

    fn num_sand_tiles(&self) -> usize {
        self.tiles.values().filter(|x| x.is_sand()).count()
    }

    fn set_tiles_between(&mut self, start: Point, end: Point, v: Tile) {
        let x_range = if start.x <= end.x {
            start.x..=end.x
        } else {
            end.x..=start.x
        };
        for x in x_range {
            let y_range = if start.y <= end.y {
                start.y..=end.y
            } else {
                end.y..=start.y
            };
            for y in y_range {
                self.set_tile((x, y).into(), v);
            }
        }
    }

    fn add_structure<T: AsRef<str>>(&mut self, structure: T) -> Result<(), CaveError> {
        let positions = structure
            .as_ref()
            .split(" -> ")
            .map(|x| x.split_once(',').expect("failed to read input data"))
            .map(|(x, y)| {
                (
                    x.parse::<usize>().expect("failed to parse an int"),
                    y.parse::<usize>().expect("failed to parse an int"),
                )
                    .into()
            })
            .collect::<Vec<_>>();
        for i in 1..positions.len() {
            self.set_tiles_between(positions[i - 1], positions[i], Tile::Rock);
        }
        Ok(())
    }

    fn from_structures<T: BufRead>(reader: T) -> Result<Self, CaveError> {
        let mut cave = Self::new();

        for line in reader.lines() {
            let line = line.expect("failed to read input data");
            cave.add_structure(line)?;
        }

        Ok(cave)
    }

    fn set_floor_level(&mut self, floor_level: usize) {
        self.floor_level = Some(floor_level);
        self.extend.max_y = floor_level;
    }
}

fn show_cave(cave: &Cave) -> String {
    let mut s = String::new();
    for y in cave.extend.min_y..cave.extend.max_y {
        for x in cave.extend.min_x..cave.extend.max_x {
            s.push(match cave.get(&Point { x, y }).unwrap_or(&Tile::Air) {
                Tile::Air => '.',
                Tile::Rock => '#',
                Tile::Sand => 'o',
            });
        }
        s.push('\n');
    }
    s
}

fn main() -> anyhow::Result<()> {
    let file = fs::File::open("input.txt")?;
    let reader = io::BufReader::new(file);
    let mut cave = Cave::from_structures(reader)?;
    while let DropOutcome::Settled = cave.drop_sand(Point { x: 500, y: 0 }) {}
    println!("part1: {}", cave.num_sand_tiles());

    let file = fs::File::open("input.txt")?;
    let reader = io::BufReader::new(file);
    let mut cave = Cave::from_structures(reader)?;
    cave.set_floor_level(cave.extend.max_y + 2);
    while let DropOutcome::Settled = cave.drop_sand(Point { x: 500, y: 0 }) {}
    println!("part2: {}", cave.num_sand_tiles());

    Ok(())
}
