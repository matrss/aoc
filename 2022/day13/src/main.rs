use derive_more::{From, IsVariant, Unwrap};
use std::cmp::Ordering;
use std::fmt;
use std::fs;
use std::str::FromStr;
use thiserror::Error;

#[derive(Clone, Copy, From, Debug)]
struct ListId(usize);

#[derive(IsVariant, Unwrap, Debug)]
enum ListItem {
    Value(i32),
    List(ListId),
}

#[derive(Debug)]
struct List(Vec<Vec<ListItem>>);

impl List {
    fn new() -> Self {
        List(Vec::new())
    }
}

impl PartialEq for List {
    fn eq(&self, other: &Self) -> bool {
        use ListItem::*;
        let mut self_current_list_id = ListId(0);
        let mut self_previous_list_ids = Vec::new();
        let mut other_current_list_id = ListId(0);
        let mut other_previous_list_ids = Vec::new();
        let mut i = 0;
        loop {
            match (
                self.0[self_current_list_id.0].get(i),
                other.0[other_current_list_id.0].get(i),
            ) {
                (Some(a), Some(b)) => match (a, b) {
                    (Value(x), Value(y)) => {
                        if x != y {
                            return false;
                        }
                    }
                    (List(_), Value(_)) => return false,
                    (Value(_), List(_)) => return false,
                    (List(x), List(y)) => {
                        self_previous_list_ids.push((self_current_list_id, i));
                        self_current_list_id = *x;
                        other_previous_list_ids.push((other_current_list_id, i));
                        other_current_list_id = *y;
                        i = 0;
                        continue;
                    }
                },
                (Some(_), None) => return false,
                (None, Some(_)) => return false,
                (None, None) => {
                    match (self_previous_list_ids.pop(), other_previous_list_ids.pop()) {
                        (Some(x), Some(y)) => {
                            self_current_list_id = x.0;
                            other_current_list_id = y.0;
                            i = x.1;
                        }
                        (Some(_), None) => return false,
                        (None, Some(_)) => return false,
                        (None, None) => return true,
                    }
                }
            }
            i += 1;
        }
    }
}

impl Eq for List {}

impl PartialOrd for List {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for List {
    fn cmp(&self, other: &Self) -> Ordering {
        use ListItem::*;
        let mut self_current_list_id = ListId(0);
        let mut self_previous_list_ids = Vec::new();
        let mut other_current_list_id = ListId(0);
        let mut other_previous_list_ids = Vec::new();
        let mut i = 0;
        loop {
            match (
                self.0[self_current_list_id.0].get(i),
                other.0[other_current_list_id.0].get(i),
            ) {
                (Some(a), Some(b)) => match (a, b) {
                    (Value(x), Value(y)) => match x.cmp(y) {
                        Ordering::Less => return Ordering::Less,
                        Ordering::Greater => return Ordering::Greater,
                        Ordering::Equal => {}
                    },
                    (List(x_id), Value(y)) => {
                        let x = {
                            let mut x = self.0[x_id.0].get(0);
                            loop {
                                match x {
                                    Some(Value(y)) => break y,
                                    Some(List(y)) => x = self.0[y.0].get(0),
                                    None => return Ordering::Less,
                                }
                            }
                        };
                        if x > y {
                            return Ordering::Greater;
                        } else if x < y {
                            return Ordering::Less;
                        } else if self.0[x_id.0].get(1).is_some() {
                            return Ordering::Greater;
                        }
                    }
                    (Value(x), List(y_id)) => {
                        let y = {
                            let mut y = other.0[y_id.0].get(0);
                            loop {
                                match y {
                                    Some(Value(x)) => break x,
                                    Some(List(x)) => y = other.0[x.0].get(0),
                                    None => return Ordering::Greater,
                                }
                            }
                        };
                        if x > y {
                            return Ordering::Greater;
                        } else if x < y || other.0[y_id.0].get(1).is_some() {
                            return Ordering::Less;
                        }
                    }
                    (List(x), List(y)) => {
                        self_previous_list_ids.push((self_current_list_id, i));
                        self_current_list_id = *x;
                        other_previous_list_ids.push((other_current_list_id, i));
                        other_current_list_id = *y;
                        i = 0;
                        continue;
                    }
                },
                (Some(_), None) => return Ordering::Greater,
                (None, Some(_)) => return Ordering::Less,
                (None, None) => {
                    match (self_previous_list_ids.pop(), other_previous_list_ids.pop()) {
                        (Some(x), Some(y)) => {
                            self_current_list_id = x.0;
                            other_current_list_id = y.0;
                            i = x.1;
                        }
                        (Some(_), None) => return Ordering::Greater,
                        (None, Some(_)) => return Ordering::Less,
                        (None, None) => return Ordering::Equal,
                    }
                }
            }
            i += 1;
        }
    }
}

#[derive(Error, Debug)]
enum ParseListError {}

impl FromStr for List {
    type Err = ParseListError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut list = List::new();
        list.0.push(Vec::new());
        let mut current_list_id = ListId(0);
        let mut previous_list_ids = Vec::new();
        let s = s
            .strip_prefix('[')
            .unwrap_or(s)
            .strip_suffix(']')
            .unwrap_or(s);
        for mut e in s.split(',') {
            while e.starts_with('[') {
                list.0.push(Vec::new());
                let new_list_id = ListId(list.0.len() - 1);
                list.0[current_list_id.0].push(ListItem::List(new_list_id));
                previous_list_ids.push(current_list_id);
                current_list_id = new_list_id;
                e = e.strip_prefix('[').unwrap();
            }
            let mut end_lists = 0;
            while e.ends_with(']') {
                end_lists += 1;
                e = e.strip_suffix(']').unwrap();
            }
            if let Ok(x) = e.parse::<i32>() {
                list.0[current_list_id.0].push(ListItem::Value(x));
            }
            for _ in 0..end_lists {
                current_list_id = previous_list_ids.pop().unwrap();
            }
        }
        Ok(list)
    }
}

impl fmt::Display for List {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn _fmt(
            f: &mut fmt::Formatter<'_>,
            a: &Vec<Vec<ListItem>>,
            l: &Vec<ListItem>,
        ) -> fmt::Result {
            for e in l {
                match e {
                    ListItem::Value(x) => write!(f, "{}", x)?,
                    ListItem::List(id) => {
                        write!(f, "[")?;
                        _fmt(f, a, &a[id.0])?;
                        write!(f, "]")?;
                    }
                }
            }
            Ok(())
        }
        write!(f, "[")?;
        _fmt(f, &self.0, &self.0[0])?;
        write!(f, "]")?;
        Ok(())
    }
}

#[derive(Error, Debug)]
enum ProgramError {
    #[error("invalid input data encountered")]
    InvalidInputData,
}

fn main() -> anyhow::Result<()> {
    let input = fs::read_to_string("input.txt")?;
    let input = input.strip_suffix('\n').unwrap();

    let mut sum_idx_correct_order = 0;
    for (idx, pair) in input.split("\n\n").enumerate() {
        let (first, second) = pair
            .split_once('\n')
            .ok_or(ProgramError::InvalidInputData)?;
        let first = first.parse::<List>()?;
        let second = second.parse::<List>()?;
        if first <= second {
            sum_idx_correct_order += idx + 1;
        }
    }
    println!("part1: {}", sum_idx_correct_order);

    let mut packets = input
        .split('\n')
        .filter(|x| !x.is_empty())
        .map(|x| x.parse::<List>().unwrap())
        .collect::<Vec<_>>();
    packets.push("[[2]]".parse::<List>()?);
    packets.push("[[6]]".parse::<List>()?);
    packets.sort();
    let first_divider_idx = packets
        .iter()
        .position(|x| x == &"[[2]]".parse::<List>().unwrap())
        .unwrap()
        + 1;
    let second_divider_idx = packets
        .iter()
        .position(|x| x == &"[[6]]".parse::<List>().unwrap())
        .unwrap()
        + 1;
    let decoder_key = first_divider_idx * second_divider_idx;
    println!("part2: {}", decoder_key);

    Ok(())
}
