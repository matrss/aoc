use std::fs;
use std::io::{self, prelude::*};

fn common_item_in(compartment1: &str, compartment2: &str) -> Option<char> {
    for c in compartment1.chars() {
        if compartment2.contains(c) {
            return Some(c);
        }
    }
    None
}

fn char_to_prio(c: char) -> u32 {
    match c as u32 {
        c if ('a' as u32) <= c && c <= ('z' as u32) => c - ('a' as u32) + 1,
        c if ('A' as u32) <= c && c <= ('Z' as u32) => c - ('A' as u32) + 27,
        _ => panic!(),
    }
}

fn badge_item_in(rucksack1: &str, rucksack2: &str, rucksack3: &str) -> Option<char> {
    let common_in_1_and_2 = rucksack1.chars().filter(|x| rucksack2.contains(*x));
    let badge = common_in_1_and_2.filter(|x| rucksack3.contains(*x)).next();
    badge
}

fn main() {
    let file = fs::File::open("input.txt").unwrap();
    let reader = io::BufReader::new(file);

    let mut total_item_priority = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let size = line.len();
        let compartment1 = &line[..size / 2];
        let compartment2 = &line[size / 2..];
        let common_item = common_item_in(compartment1, compartment2).unwrap();
        let char_prio = char_to_prio(common_item);
        total_item_priority = total_item_priority + char_prio;
    }

    println!("part1: {}", total_item_priority);

    let file = fs::File::open("input.txt").unwrap();
    let reader = io::BufReader::new(file);

    let mut iter = reader.lines().peekable();
    let mut total_item_priority = 0;
    loop {
        if iter.peek().is_none() {
            break;
        }
        let rucksack1 = iter.next().unwrap().unwrap();
        let rucksack2 = iter.next().unwrap().unwrap();
        let rucksack3 = iter.next().unwrap().unwrap();
        let badge = badge_item_in(&rucksack1, &rucksack2, &rucksack3).unwrap();
        let char_prio = char_to_prio(badge);
        total_item_priority = total_item_priority + char_prio;
    }

    println!("part2: {}", total_item_priority);
}
