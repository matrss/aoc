use std::fs;

fn main() {
    let input_path = "input.txt";
    let content = fs::read_to_string(input_path).unwrap();
    let mut calories_per_elf = content
        .split("\n\n")
        .map(|elf_inventory| {
            elf_inventory
                .split("\n")
                .filter_map(|x| x.parse::<u32>().ok())
                .sum::<u32>()
        })
        .collect::<Vec<_>>();
    let most_calories = calories_per_elf.iter().copied().max().unwrap();
    calories_per_elf.sort();
    let top_three_sum = calories_per_elf.iter().copied().rev().take(3).sum::<u32>();

    println!("most calories: {}", most_calories);
    println!(
        "cumulative calories of the top three elves: {}",
        top_three_sum
    );
}
