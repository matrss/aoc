use std::fs;
use std::io::{self, prelude::*};
use std::num;
use std::str::FromStr;

#[derive(Debug)]
struct Crate(char);

#[derive(Debug)]
struct Stacks {
    stacks: Vec<Vec<Crate>>,
}

#[derive(Debug)]
enum ParseStacksError {
    WrongFormat,
}

#[derive(Debug)]
enum StacksError {
    EmptyFromStack,
}

impl FromStr for Stacks {
    type Err = ParseStacksError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use ParseStacksError::*;
        let mut stacks: Vec<Vec<Crate>> = vec![];
        let num_stacks = (s.lines().next().ok_or(WrongFormat)?.len() + 1) / 4;
        for _ in 0..num_stacks {
            let stack: Vec<Crate> = vec![];
            stacks.push(stack);
        }
        for line in s.lines().filter(|x| x.starts_with("[")).rev() {
            for i in 0..num_stacks {
                let c = line.as_bytes()[i * 4 + 1] as char;
                if c != ' ' {
                    stacks[i].push(Crate(c));
                }
            }
        }
        Ok(Stacks { stacks })
    }
}

impl Stacks {
    fn move_crates_single(
        &mut self,
        count: usize,
        from: usize,
        to: usize,
    ) -> Result<(), StacksError> {
        use StacksError::*;
        for _ in 0..count {
            let c = self.stacks[from].pop().ok_or(EmptyFromStack)?;
            self.stacks[to].push(c);
        }
        Ok(())
    }

    fn move_crates_multiple(
        &mut self,
        count: usize,
        from: usize,
        to: usize,
    ) -> Result<(), StacksError> {
        let idx = self.stacks[from].len() - count;
        let c = self.stacks[from].drain(idx..).collect::<Vec<_>>();
        self.stacks[to].extend(c);
        Ok(())
    }

    fn tops(&self) -> String {
        let mut tops = String::new();
        for stack in &self.stacks {
            tops.push(stack.last().map_or(' ', |x| x.0));
        }
        tops
    }
}

#[derive(Debug)]
enum Error {
    IO(io::Error),
    Stacks(StacksError),
    ParseStacks(ParseStacksError),
    ParseInt(num::ParseIntError),
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::IO(err)
    }
}

impl From<StacksError> for Error {
    fn from(err: StacksError) -> Self {
        Self::Stacks(err)
    }
}

impl From<ParseStacksError> for Error {
    fn from(err: ParseStacksError) -> Self {
        Self::ParseStacks(err)
    }
}

impl From<num::ParseIntError> for Error {
    fn from(err: num::ParseIntError) -> Self {
        Self::ParseInt(err)
    }
}

fn main() -> Result<(), Error> {
    let file = fs::File::open("input.txt")?;
    let reader = io::BufReader::new(file);
    let mut lines = reader.lines();
    let stacks_str = lines
        .by_ref()
        .flatten()
        .take_while(|x| x != "")
        .map(|x| x + "\n")
        .collect::<String>();
    let mut stacks_part1 = stacks_str.parse::<Stacks>()?;
    let mut stacks_part2 = stacks_str.parse::<Stacks>()?;

    for line in lines {
        let line = line?;
        let line_split = line.split(' ').collect::<Vec<_>>();
        let count = line_split[1].parse::<usize>()?;
        let from = line_split[3].parse::<usize>()? - 1;
        let to = line_split[5].parse::<usize>()? - 1;
        stacks_part1.move_crates_single(count, from, to)?;
        stacks_part2.move_crates_multiple(count, from, to)?;
    }

    println!("part1: {}", stacks_part1.tops());
    println!("part2: {}", stacks_part2.tops());
    Ok(())
}
