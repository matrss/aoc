use derive_more::{Add, AddAssign, From, Index};
use std::fs;
use std::{collections::HashMap, hash::Hash};
use thiserror::Error;

#[derive(Error, Debug)]
enum ProgramError {
    #[error("encountered an invalid elevation value")]
    InvalidElevation,
    #[error("could not find element in graph")]
    ElementNotFound,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Add, From, Debug)]
struct Elevation(u8);

impl Elevation {
    fn from_char(c: char) -> Result<Self, ProgramError> {
        match c {
            'a' => Ok(Elevation(0)),
            'b' => Ok(Elevation(1)),
            'c' => Ok(Elevation(2)),
            'd' => Ok(Elevation(3)),
            'e' => Ok(Elevation(4)),
            'f' => Ok(Elevation(5)),
            'g' => Ok(Elevation(6)),
            'h' => Ok(Elevation(7)),
            'i' => Ok(Elevation(8)),
            'j' => Ok(Elevation(9)),
            'k' => Ok(Elevation(10)),
            'l' => Ok(Elevation(11)),
            'm' => Ok(Elevation(12)),
            'n' => Ok(Elevation(13)),
            'o' => Ok(Elevation(14)),
            'p' => Ok(Elevation(15)),
            'q' => Ok(Elevation(16)),
            'r' => Ok(Elevation(17)),
            's' => Ok(Elevation(18)),
            't' => Ok(Elevation(19)),
            'u' => Ok(Elevation(20)),
            'v' => Ok(Elevation(21)),
            'w' => Ok(Elevation(22)),
            'x' => Ok(Elevation(23)),
            'y' => Ok(Elevation(24)),
            'z' => Ok(Elevation(25)),
            _ => Err(ProgramError::InvalidElevation),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Add, AddAssign, From, Debug)]
struct NodeId(usize);

#[derive(Debug)]
struct Node {
    elevation: Elevation,
}

impl Node {
    fn new(elevation: Elevation) -> Self {
        Node { elevation }
    }
}

#[derive(Index, Debug)]
struct Graph(HashMap<NodeId, (Node, Vec<NodeId>)>);

impl Graph {
    fn new() -> Self {
        Graph(HashMap::new())
    }

    fn keys(&self) -> std::collections::hash_map::Keys<NodeId, (Node, Vec<NodeId>)> {
        self.0.keys()
    }

    fn insert(&mut self, idx: NodeId, elem: Node) {
        self.0.insert(idx, (elem, Vec::new()));
    }

    fn set_connections(
        &mut self,
        idx: &NodeId,
        connections: Vec<NodeId>,
    ) -> Result<(), ProgramError> {
        let entry = self.0.get_mut(idx).ok_or(ProgramError::ElementNotFound)?;
        entry.1 = connections;
        Ok(())
    }
}

fn dijkstra(
    graph: &Graph,
    start_id: &NodeId,
) -> (
    HashMap<NodeId, Option<u32>>,
    HashMap<NodeId, Option<NodeId>>,
) {
    let mut distances = graph
        .keys()
        .map(|x| (*x, None))
        .collect::<HashMap<_, Option<u32>>>();
    distances.insert(*start_id, Some(0));
    let mut previous = graph
        .keys()
        .map(|x| (*x, None))
        .collect::<HashMap<_, Option<NodeId>>>();
    let mut is_visited = graph.keys().map(|x| (*x, false)).collect::<HashMap<_, _>>();
    loop {
        let x = distances
            .iter()
            .filter(|x| !is_visited[x.0] && x.1.is_some())
            .map(|x| (*x.0, x.1.unwrap()))
            .min_by_key(|x| x.1);
        if x.is_none() {
            break;
        }
        let x = x.unwrap();
        is_visited.insert(x.0, true);

        for neighbor_id in &graph[&x.0].1 {
            let neighbor_dist_entry = distances[neighbor_id];
            let new_dist = match neighbor_dist_entry {
                None => {
                    previous.insert(*neighbor_id, Some(x.0));
                    distances[&x.0].unwrap() + 1
                }
                Some(v) => {
                    let possible_dist = distances[&x.0].unwrap() + 1;
                    if possible_dist < v {
                        previous.insert(*neighbor_id, Some(x.0));
                        possible_dist
                    } else {
                        v
                    }
                }
            };
            distances.insert(*neighbor_id, Some(new_dist));
        }
    }
    (distances, previous)
}

fn build_graph<F: Fn(&Node, &Node) -> bool>(
    s: &str,
    predicate: F,
) -> Result<(Graph, NodeId, NodeId), ProgramError> {
    // Generate node objects
    let mut graph = Graph::new();
    let mut rows = 0;
    let mut columns = 0;
    let mut curr_id = NodeId(0);
    let mut start_id = None;
    let mut end_id = None;
    for line in s.lines() {
        rows += 1;
        columns = 0;
        for c in line.chars() {
            columns += 1;
            let elevation_char = if c == 'S' {
                start_id = Some(curr_id);
                'a'
            } else if c == 'E' {
                end_id = Some(curr_id);
                'z'
            } else {
                c
            };
            let node = Node::new(Elevation::from_char(elevation_char)?);
            graph.insert(curr_id, node);
            curr_id += 1.into();
        }
    }
    let start_id = start_id.unwrap();
    let end_id = end_id.unwrap();

    // Add connections to graph
    for row in 0..rows {
        for column in 0..columns {
            let neighbors = [
                if column == 0 {
                    None
                } else {
                    let id = NodeId(row * columns + column - 1);
                    Some((id, &graph[&id]))
                },
                if column == columns - 1 {
                    None
                } else {
                    let id = NodeId(row * columns + column + 1);
                    Some((id, &graph[&id]))
                },
                if row == 0 {
                    None
                } else {
                    let id = NodeId((row - 1) * columns + column);
                    Some((id, &graph[&id]))
                },
                if row == rows - 1 {
                    None
                } else {
                    let id = NodeId((row + 1) * columns + column);
                    Some((id, &graph[&id]))
                },
            ];
            let node_id = NodeId(row * columns + column);
            let node = &graph[&node_id];
            let mut edges = Vec::new();
            for neighbor in neighbors.iter().filter_map(|x| *x) {
                if predicate(&node.0, &neighbor.1 .0) {
                    edges.push(neighbor.0);
                }
            }
            graph.set_connections(&node_id, edges)?;
        }
    }
    Ok((graph, start_id, end_id))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn min_distance_test_input_part1() {
        let input = fs::read_to_string("test_input.txt").unwrap();
        let (graph, start_id, end_id) = build_graph(&input, |node, neighbor| {
            neighbor.elevation <= node.elevation + 1.into()
        })
        .unwrap();
        let (distances, _) = dijkstra(&graph, &start_id);
        let min_distance = distances[&end_id].unwrap();
        assert_eq!(min_distance, 31);
    }

    #[test]
    fn min_distance_test_input_part2() {
        let input = fs::read_to_string("test_input.txt").unwrap();
        let (graph, _start_id, end_id) = build_graph(&input, |node, neighbor| {
            node.elevation <= neighbor.elevation + 1.into()
        })
        .unwrap();
        let (distances, _) = dijkstra(&graph, &end_id);
        let min_distance = distances
            .iter()
            .filter(|x| graph[x.0].0.elevation == 0.into())
            .filter_map(|x| *x.1)
            .min()
            .unwrap();
        assert_eq!(min_distance, 29);
    }
}

fn main() -> anyhow::Result<()> {
    let input = fs::read_to_string("input.txt")?;

    let (graph, start_id, end_id) = build_graph(&input, |node, neighbor| {
        neighbor.elevation <= node.elevation + 1.into()
    })?;
    let (distances, _) = dijkstra(&graph, &start_id);
    let min_distance = distances[&end_id].unwrap();
    println!("part1: {}", min_distance);

    let (graph, _start_id, end_id) = build_graph(&input, |node, neighbor| {
        node.elevation <= neighbor.elevation + 1.into()
    })
    .unwrap();
    let (distances, _) = dijkstra(&graph, &end_id);
    let min_distance = distances
        .iter()
        .filter(|x| graph[x.0].0.elevation == 0.into())
        .filter_map(|x| *x.1)
        .min()
        .unwrap();
    println!("part2: {}", min_distance);
    Ok(())
}
