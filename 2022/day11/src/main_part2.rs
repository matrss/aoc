use derive_more::From;
use std::collections::HashMap;
use std::fs;
use std::num::ParseIntError;
use std::str::FromStr;
use thiserror::Error;

#[derive(Clone, Debug)]
struct Item {
    initial_value: i32,
    remainders: HashMap<i32, i32>,
}

#[derive(Error, Debug)]
enum ItemError {
    #[error("remainder not found")]
    RemainderNotFound,
}

impl Item {
    fn new(initial_value: i32) -> Self {
        let remainders = HashMap::new();
        Self {
            initial_value,
            remainders,
        }
    }

    fn update_remainders(&mut self) -> Result<(), ItemError> {
        let divisors = self
            .remainders
            .keys()
            .map(|x| x.to_owned())
            .collect::<Vec<_>>();
        for divisor in divisors {
            let remainder = self
                .remainders
                .get(&divisor)
                .ok_or(ItemError::RemainderNotFound)?;
            self.remainders.insert(divisor, remainder % divisor);
        }
        Ok(())
    }
}

#[derive(Clone, Debug)]
enum Operation {
    Add(i32),
    Mul(i32),
    Div(i32),
    Square,
    NoOp,
}

impl Operation {
    fn apply(&self, item: &mut Item) {
        use Operation::*;
        for e in item.remainders.values_mut() {
            match self {
                Add(x) => *e += x,
                Mul(x) => *e *= x,
                Div(x) => *e /= x,
                Square => *e *= *e,
                NoOp => {}
            }
        }
    }
}

#[derive(Error, Debug)]
enum ParseOperationError {
    #[error("failed to parse input data")]
    InvalidData,
    #[error("failed to parse int")]
    ParseInt(#[from] ParseIntError),
}

impl FromStr for Operation {
    type Err = ParseOperationError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let e = s
            .strip_prefix("new = ")
            .ok_or(Self::Err::InvalidData)?
            .split(' ')
            .collect::<Vec<_>>();
        if e[0] == "old" && e[1] == "*" && e[2] == "old" {
            Ok(Operation::Square)
        } else if e[0] == "old" && e[1] == "*" {
            let val = e[2].parse::<i32>()?;
            Ok(Operation::Mul(val))
        } else if e[0] == "old" && e[1] == "+" {
            let val = e[2].parse::<i32>()?;
            Ok(Operation::Add(val))
        } else {
            Err(Self::Err::InvalidData)
        }
    }
}

#[derive(Clone, Debug)]
enum TestCondition {
    DivisibleBy(i32),
}

#[derive(Error, Debug)]
enum ParseTestConditionError {
    #[error("failed to parse input data")]
    InvalidData,
    #[error("failed to parse int")]
    ParseInt(#[from] ParseIntError),
}

impl FromStr for TestCondition {
    type Err = ParseTestConditionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(x) = s.strip_prefix("divisible by ") {
            Ok(Self::DivisibleBy(x.parse::<i32>()?))
        } else {
            Err(Self::Err::InvalidData)
        }
    }
}

#[derive(Clone, Debug)]
struct Test {
    condition: TestCondition,
    action_true: MonkeyId,
    action_false: MonkeyId,
}

#[derive(Error, Debug)]
enum TestError {
    #[error("remainder not found on item")]
    RemainderNotFoundOnItem,
}

impl Test {
    fn test(&self, e: &Item) -> Result<MonkeyId, TestError> {
        use TestCondition::*;
        match self.condition {
            DivisibleBy(x) => {
                let remainder = *e
                    .remainders
                    .get(&x)
                    .ok_or(TestError::RemainderNotFoundOnItem)?;
                if remainder == 0 {
                    Ok(self.action_true)
                } else {
                    Ok(self.action_false)
                }
            }
        }
    }
}

#[derive(From, PartialEq, Copy, Clone, Debug)]
struct MonkeyId(u32);

#[derive(Clone, Debug)]
struct Monkey {
    id: MonkeyId,
    items: Vec<Item>,
    operation: Operation,
    test: Test,
    total_inspected: u64,
}

#[derive(Error, Debug)]
enum ParseMonkeyError {
    #[error("failed to parse input data")]
    InvalidData,
    #[error("failed to parse operation")]
    ParseOperation(#[from] ParseOperationError),
    #[error("failed to parse int")]
    ParseInt(#[from] ParseIntError),
    #[error("failed to parse test condition")]
    ParseTestCondition(#[from] ParseTestConditionError),
}

impl FromStr for Monkey {
    type Err = ParseMonkeyError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.lines();
        let line = lines.next().ok_or(Self::Err::InvalidData)?;
        let id = line
            .strip_prefix("Monkey ")
            .ok_or(Self::Err::InvalidData)?
            .strip_suffix(':')
            .ok_or(Self::Err::InvalidData)?
            .parse::<u32>()?
            .into();
        let line = lines.next().ok_or(Self::Err::InvalidData)?;
        let items_str = line
            .strip_prefix("  Starting items: ")
            .ok_or(Self::Err::InvalidData)?;
        let items = items_str
            .split(", ")
            .map(|x| Item::new(x.parse::<i32>().unwrap()))
            .collect::<Vec<_>>();
        let line = lines.next().ok_or(Self::Err::InvalidData)?;
        let operation = line
            .strip_prefix("  Operation: ")
            .ok_or(Self::Err::InvalidData)?
            .parse::<Operation>()?;
        let line = lines.next().ok_or(Self::Err::InvalidData)?;
        let test_condition = line
            .strip_prefix("  Test: ")
            .ok_or(Self::Err::InvalidData)?
            .parse::<TestCondition>()?;
        let line = lines.next().ok_or(Self::Err::InvalidData)?;
        let test_action_true = line
            .strip_prefix("    If true: throw to monkey ")
            .ok_or(Self::Err::InvalidData)?
            .parse::<u32>()?
            .into();
        let line = lines.next().ok_or(Self::Err::InvalidData)?;
        let test_action_false = line
            .strip_prefix("    If false: throw to monkey ")
            .ok_or(Self::Err::InvalidData)?
            .parse::<u32>()?
            .into();
        let test = Test {
            condition: test_condition,
            action_true: test_action_true,
            action_false: test_action_false,
        };
        Ok(Monkey {
            id,
            items,
            operation,
            test,
            total_inspected: 0,
        })
    }
}

struct KeepAwayGame {
    monkeys: Vec<Monkey>,
    relief_operation: Operation,
}

#[derive(Error, Debug)]
enum KeepAwayGameError {
    #[error("could not find monkey")]
    MonkeyNotFound,
    #[error("error in application of test")]
    Test(#[from] TestError),
    #[error("item error")]
    Item(#[from] ItemError),
}

impl KeepAwayGame {
    fn new() -> Self {
        let monkeys: Vec<Monkey> = Vec::new();
        let relief_operation = Operation::Div(3);
        Self {
            monkeys,
            relief_operation,
        }
    }

    fn play_round(&mut self) -> Result<(), KeepAwayGameError> {
        for i in 0..self.monkeys.len() {
            for j in 0..self.monkeys[i].items.len() {
                let monkey = &mut self.monkeys[i];
                let mut item = monkey.items[j].clone();
                monkey.operation.apply(&mut item);
                self.relief_operation.apply(&mut item);
                item.update_remainders()?;
                let target = monkey.test.test(&item)?;
                let target_monkey = self
                    .monkeys
                    .iter_mut()
                    .find(|x| x.id == target)
                    .ok_or(KeepAwayGameError::MonkeyNotFound)?;
                target_monkey.items.push(item);
            }
            let monkey = &mut self.monkeys[i];
            let inspected = monkey.items.len();
            monkey.items = Vec::new();
            monkey.total_inspected += inspected as u64;
        }
        Ok(())
    }

    fn play_rounds(&mut self, num_rounds: u32) -> Result<(), KeepAwayGameError> {
        for _ in 0..num_rounds {
            self.play_round()?;
        }
        Ok(())
    }

    fn monkey_business_level(&self) -> u64 {
        let mut monkeys = self.monkeys.clone();
        let len = monkeys.len();
        monkeys.sort_by_key(|e| e.total_inspected);
        monkeys[len - 1].total_inspected * monkeys[len - 2].total_inspected
    }
}

#[derive(Error, Debug)]
enum ParseKeepAwayGameError {
    #[error("failed to parse monkey")]
    ParseMonkey(#[from] ParseMonkeyError),
    #[error("item error")]
    Item(#[from] ItemError),
}

impl FromStr for KeepAwayGame {
    type Err = ParseKeepAwayGameError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut keep_away = KeepAwayGame::new();
        for e in s.split("\n\n") {
            let monkey = e.parse::<Monkey>()?;
            keep_away.monkeys.push(monkey);
        }
        let divisors = keep_away
            .monkeys
            .iter()
            .map(|x| match x.test.condition {
                TestCondition::DivisibleBy(x) => x,
            })
            .collect::<Vec<_>>();
        for monkey in &mut keep_away.monkeys {
            for item in &mut monkey.items {
                for divisor in divisors.iter() {
                    item.remainders.insert(*divisor, item.initial_value);
                }
                item.update_remainders()?;
            }
        }
        Ok(keep_away)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn monkey_business_level_input1_part2() {
        let input = fs::read_to_string("test_input.txt").expect("Failed to read test_input.txt");
        let mut keep_away = input
            .parse::<KeepAwayGame>()
            .expect("Failed to parse test_input.txt");
        keep_away.relief_operation = Operation::NoOp;
        keep_away.play_rounds(10000).expect("Failed playing rounds");
        assert_eq!(keep_away.monkey_business_level(), 2713310158);
    }
}

fn main() -> anyhow::Result<()> {
    // let input = fs::read_to_string("input.txt")?;
    // let mut keep_away = input.parse::<KeepAwayGame>()?;
    // keep_away.play_rounds(20)?;
    // println!("part1: {}", keep_away.monkey_business_level());

    let input = fs::read_to_string("input.txt")?;
    let mut keep_away = input.parse::<KeepAwayGame>()?;
    keep_away.relief_operation = Operation::NoOp;
    keep_away.play_rounds(10000)?;
    println!("part2: {}", keep_away.monkey_business_level());
    Ok(())
}
