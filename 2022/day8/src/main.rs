use std::fs;
use std::io;

#[derive(Debug)]
struct Tree {
    visible_top: Visibility,
    view_distance_top: usize,
    visible_bottom: Visibility,
    view_distance_bottom: usize,
    visible_left: Visibility,
    view_distance_left: usize,
    visible_right: Visibility,
    view_distance_right: usize,
}

impl Tree {
    fn new(
        visible_top: Visibility,
        view_distance_top: usize,
        visible_bottom: Visibility,
        view_distance_bottom: usize,
        visible_left: Visibility,
        view_distance_left: usize,
        visible_right: Visibility,
        view_distance_right: usize,
    ) -> Self {
        Tree {
            visible_top,
            view_distance_top,
            visible_bottom,
            view_distance_bottom,
            visible_left,
            view_distance_left,
            visible_right,
            view_distance_right,
        }
    }

    fn is_visible_at_all(&self) -> bool {
        use Visibility::*;
        !matches!(
            (
                &self.visible_top,
                &self.visible_bottom,
                &self.visible_left,
                &self.visible_right,
            ),
            (Hidden, Hidden, Hidden, Hidden)
        )
    }

    fn scenic_score(&self) -> usize {
        self.view_distance_top
            * self.view_distance_bottom
            * self.view_distance_left
            * self.view_distance_right
    }
}

#[derive(PartialEq, Debug)]
enum Visibility {
    Visible,
    Hidden,
}

fn main() -> io::Result<()> {
    let grid_str = fs::read_to_string("input.txt")?;
    let lines = grid_str.lines().collect::<Vec<_>>();
    let rows = lines.len();
    let columns = lines[0].chars().count();

    let mut height_grid: Vec<u32> = Vec::with_capacity(rows * columns);

    for line in lines {
        for tree_char in line.chars() {
            height_grid.push(
                tree_char.to_digit(10).ok_or_else(|| {
                    io::Error::new(io::ErrorKind::InvalidInput, "Invalid tree char")
                })?,
            );
        }
    }

    let mut tree_grid: Vec<Tree> = Vec::with_capacity(rows * columns);

    for row in 0..rows {
        for column in 0..columns {
            let current_height = height_grid[row * columns + column];

            let trees_to_top_edge = height_grid[column..row * columns + column]
                .iter()
                .step_by(columns)
                .rev()
                .collect::<Vec<_>>();
            let visible_top =
                if row == 0 || !trees_to_top_edge.iter().any(|&&x| x >= current_height) {
                    Visibility::Visible
                } else {
                    Visibility::Hidden
                };
            let view_distance_top = if row == 0 {
                0
            } else {
                let x = trees_to_top_edge
                    .iter()
                    .take_while(|&&&x| x < current_height)
                    .count();
                if x == trees_to_top_edge.len() {
                    x
                } else {
                    x + 1
                }
            };

            let trees_to_bottom_edge = height_grid
                .get((row + 1) * columns + column..)
                .unwrap_or(&[0_u32; 0])
                .iter()
                .step_by(columns)
                .collect::<Vec<_>>();
            let visible_bottom =
                if row == rows - 1 || !trees_to_bottom_edge.iter().any(|&&x| x >= current_height) {
                    Visibility::Visible
                } else {
                    Visibility::Hidden
                };
            let view_distance_bottom = if row == rows - 1 {
                0
            } else {
                let x = trees_to_bottom_edge
                    .iter()
                    .take_while(|&&&x| x < current_height)
                    .count();
                if x == trees_to_bottom_edge.len() {
                    x
                } else {
                    x + 1
                }
            };

            let trees_to_left_edge = height_grid[row * columns..row * columns + column]
                .iter()
                .rev()
                .collect::<Vec<_>>();
            let visible_left =
                if column == 0 || !trees_to_left_edge.iter().any(|&&x| x >= current_height) {
                    Visibility::Visible
                } else {
                    Visibility::Hidden
                };
            let view_distance_left = if column == 0 {
                0
            } else {
                let x = trees_to_left_edge
                    .iter()
                    .take_while(|&&&x| x < current_height)
                    .count();
                if x == trees_to_left_edge.len() {
                    x
                } else {
                    x + 1
                }
            };

            let trees_to_right_edge = height_grid[row * columns + column + 1..(row + 1) * columns]
                .iter()
                .collect::<Vec<_>>();
            let visible_right = if column == columns - 1
                || !trees_to_right_edge.iter().any(|&&x| x >= current_height)
            {
                Visibility::Visible
            } else {
                Visibility::Hidden
            };
            let view_distance_right = if column == columns - 1 {
                0
            } else {
                let x = trees_to_right_edge
                    .iter()
                    .take_while(|&&&x| x < current_height)
                    .count();
                if x == trees_to_left_edge.len() {
                    x
                } else {
                    x + 1
                }
            };

            tree_grid.push(Tree::new(
                visible_top,
                view_distance_top,
                visible_bottom,
                view_distance_bottom,
                visible_left,
                view_distance_left,
                visible_right,
                view_distance_right,
            ));
        }
    }

    let total_visible_trees = tree_grid.iter().filter(|x| x.is_visible_at_all()).count();

    println!("part1: {}", total_visible_trees);

    let highest_scenic_score = tree_grid
        .iter()
        .map(|x| x.scenic_score())
        .max()
        .ok_or_else(|| io::Error::new(io::ErrorKind::InvalidInput, "No trees in input"))?;

    println!("part2: {}", highest_scenic_score);

    Ok(())
}
