use derive_more::From;
use std::fs;
use std::io::{self, prelude::*};
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Clone, Copy, Debug)]
struct Machine {
    cycle: usize,
    current_instruction: Option<Instruction>,
    x: i32,
}

#[derive(Debug)]
enum MachineError {
    NoInstruction,
}

impl Machine {
    fn new() -> Self {
        Self {
            cycle: 0,
            current_instruction: None,
            x: 1,
        }
    }

    fn with_instruction(self, instruction: Instruction) -> Self {
        Self {
            current_instruction: Some(instruction),
            ..self
        }
    }

    fn step(self) -> Result<Self, MachineError> {
        use Instruction::*;
        let instruction = self
            .current_instruction
            .as_ref()
            .ok_or(MachineError::NoInstruction)?;
        match instruction {
            NoOp(NoOpCycles::Zero) => Ok(Self {
                cycle: self.cycle + 1,
                current_instruction: None,
                ..self
            }),
            AddX(AddXCycles::Zero, x) => Ok(Self {
                cycle: self.cycle + 1,
                current_instruction: Some(AddX(AddXCycles::One, *x)),
                ..self
            }),
            AddX(AddXCycles::One, x) => Ok(Self {
                cycle: self.cycle + 1,
                current_instruction: None,
                x: self.x + x,
            }),
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum NoOpCycles {
    Zero,
}

#[derive(Clone, Copy, Debug)]
enum AddXCycles {
    Zero,
    One,
}

#[derive(Clone, Copy, Debug)]
enum Instruction {
    NoOp(NoOpCycles),
    AddX(AddXCycles, i32),
}

#[derive(From, Debug)]
enum ParseInstructionError {
    InvalidInstruction,
    ParseInt(ParseIntError),
}

impl FromStr for Instruction {
    type Err = ParseInstructionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Instruction::*;
        let parts = s.split(' ').collect::<Vec<_>>();
        match parts[0] {
            "noop" => Ok(NoOp(NoOpCycles::Zero)),
            "addx" => Ok(AddX(AddXCycles::Zero, parts[1].parse::<i32>()?)),
            _ => Err(Self::Err::InvalidInstruction),
        }
    }
}

#[derive(From, Debug)]
enum ProgramError {
    Machine(MachineError),
    IO(io::Error),
    ParseInstruction(ParseInstructionError),
}

fn run_with_trace(file_path: &str) -> Result<Vec<i32>, ProgramError> {
    let file = fs::File::open(file_path)?;
    let reader = io::BufReader::new(file);

    let mut machine = Machine::new();
    let mut x_states: Vec<i32> = Vec::new();
    x_states.push(machine.x);

    for line in reader.lines() {
        let line = line?;
        let instruction = line.parse::<Instruction>()?;
        machine = machine.with_instruction(instruction);

        while let Ok(m) = machine.step() {
            machine = m;
            x_states.push(machine.x);
        }
    }

    Ok(x_states)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn run_with_trace_input1() -> Result<(), ProgramError> {
        let result = run_with_trace("test_input1.txt")?;
        assert_eq!(vec![1, 1, 1, 4, 4, -1], result);
        Ok(())
    }

    #[test]
    fn run_with_trace_input2() -> Result<(), ProgramError> {
        let result = run_with_trace("test_input2.txt")?;
        assert_eq!(result[19], 21);
        assert_eq!(result[59], 19);
        assert_eq!(result[99], 18);
        assert_eq!(result[139], 21);
        assert_eq!(result[179], 16);
        assert_eq!(result[219], 18);
        Ok(())
    }
}

fn main() -> Result<(), ProgramError> {
    let x_states = run_with_trace("input.txt")?;

    let total_signal_strengths_part1 = 20 * x_states[19]
        + 60 * x_states[59]
        + 100 * x_states[99]
        + 140 * x_states[139]
        + 180 * x_states[179]
        + 220 * x_states[219];

    println!("part1: {}", total_signal_strengths_part1);

    println!("part2:");
    for row in 0..6 {
        for column in 0..40 {
            let cycle = row * 40 + column;
            let c = if column as i32 >= x_states[cycle] - 1 && column as i32 <= x_states[cycle] + 1
            {
                '#'
            } else {
                '.'
            };
            print!("{}", c);
        }
        println!();
    }

    Ok(())
}
