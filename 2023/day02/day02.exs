defmodule Pull do
  @enforce_keys [:red, :green, :blue]
  defstruct [:red, :green, :blue]

  def parse(str) do
    m =
      str
      |> String.split(", ")
      |> Enum.reduce(%{}, fn x, acc ->
        [n, color] = String.split(x, " ")
        Map.put_new(acc, color, String.to_integer(n))
      end)

    %Pull{red: Map.get(m, "red", 0), green: Map.get(m, "green", 0), blue: Map.get(m, "blue", 0)}
  end
end

defmodule Game do
  @enforce_keys [:id, :pulls]
  defstruct [:id, :pulls]

  def parse(str) do
    [game_spec, pull_specs] = String.split(str, ": ")
    "Game " <> id = game_spec

    pulls =
      pull_specs
      |> String.split("; ")
      |> Enum.map(fn pull_spec ->
        Pull.parse(pull_spec)
      end)

    %Game{id: id, pulls: pulls}
  end
end

defmodule Day02 do
  def call do
    [input_file] = System.argv()

    input =
      File.read!(input_file)
      |> String.split("\n")
      |> Enum.reject(fn x -> x == "" end)

    IO.write("part 1: ")
    part1(input) |> IO.puts()
    IO.write("part 2: ")
    part2(input) |> IO.puts()
  end

  defp part1(input) do
    bag_content = %{red: 12, green: 13, blue: 14}

    input
    |> Enum.map(&Game.parse/1)
    |> Enum.filter(fn x -> is_game_possible?(bag_content, x) end)
    |> Enum.map(fn x -> String.to_integer(x.id) end)
    |> Enum.sum()
  end

  defp part2(input) do
    input
    |> Enum.map(fn x ->
      game = Game.parse(x)
      %{red: red, green: green, blue: blue} = minimum_required_cubes(game.pulls)
      red * green * blue
    end)
    |> Enum.sum()
  end

  defp is_pull_possible?(bag_content, %Pull{} = pull),
    do:
      pull.red <= bag_content.red && pull.green <= bag_content.green &&
        pull.blue <= bag_content.blue

  defp is_game_possible?(bag_content, %Game{} = game),
    do: game.pulls |> Enum.map(fn x -> is_pull_possible?(bag_content, x) end) |> Enum.all?()

  defp minimum_required_cubes(pulls) do
    pulls
    |> Enum.reduce(%{red: 0, green: 0, blue: 0}, fn pull, acc ->
      acc
      |> Map.put(:red, max(pull.red, acc.red))
      |> Map.put(:green, max(pull.green, acc.green))
      |> Map.put(:blue, max(pull.blue, acc.blue))
    end)
  end
end

Day02.call()
