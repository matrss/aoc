defmodule Day01 do
  def call do
    [input_file] = System.argv()
    IO.write("part 1: ")
    part1(input_file) |> IO.puts()
    IO.write("part 2: ")
    part2(input_file) |> IO.puts()
  end

  defp part1(input_file) do
    File.read!(input_file)
    |> String.split("\n")
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.map(fn x ->
      String.graphemes(x)
      |> Enum.filter(fn c ->
        try do
          String.to_integer(c)
        rescue
          _ -> false
        end
      end)
    end)
    |> Enum.map(fn x -> Enum.at(x, 0) <> Enum.at(x, -1) end)
    |> Enum.map(&String.to_integer/1)
    |> Enum.sum()
  end

  defp part2(input_file) do
    content = File.read!(input_file)

    content
    |> String.split("\n")
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.map(fn x ->
      first =
        x
        |> String.graphemes()
        |> Enum.reduce_while("", fn x, acc ->
          y = x <> acc

          case {x, y} do
            {_, "eno" <> _} -> {:halt, "1"}
            {_, "owt" <> _} -> {:halt, "2"}
            {_, "eerht" <> _} -> {:halt, "3"}
            {_, "ruof" <> _} -> {:halt, "4"}
            {_, "evif" <> _} -> {:halt, "5"}
            {_, "xis" <> _} -> {:halt, "6"}
            {_, "neves" <> _} -> {:halt, "7"}
            {_, "thgie" <> _} -> {:halt, "8"}
            {_, "enin" <> _} -> {:halt, "9"}
            {"1", _} -> {:halt, "1"}
            {"2", _} -> {:halt, "2"}
            {"3", _} -> {:halt, "3"}
            {"4", _} -> {:halt, "4"}
            {"5", _} -> {:halt, "5"}
            {"6", _} -> {:halt, "6"}
            {"7", _} -> {:halt, "7"}
            {"8", _} -> {:halt, "8"}
            {"9", _} -> {:halt, "9"}
            _ -> {:cont, y}
          end
        end)

      last =
        x
        |> String.reverse()
        |> String.graphemes()
        |> Enum.reduce_while("", fn x, acc ->
          y = x <> acc

          case {x, y} do
            {_, "one" <> _} -> {:halt, "1"}
            {_, "two" <> _} -> {:halt, "2"}
            {_, "three" <> _} -> {:halt, "3"}
            {_, "four" <> _} -> {:halt, "4"}
            {_, "five" <> _} -> {:halt, "5"}
            {_, "six" <> _} -> {:halt, "6"}
            {_, "seven" <> _} -> {:halt, "7"}
            {_, "eight" <> _} -> {:halt, "8"}
            {_, "nine" <> _} -> {:halt, "9"}
            {"1", _} -> {:halt, "1"}
            {"2", _} -> {:halt, "2"}
            {"3", _} -> {:halt, "3"}
            {"4", _} -> {:halt, "4"}
            {"5", _} -> {:halt, "5"}
            {"6", _} -> {:halt, "6"}
            {"7", _} -> {:halt, "7"}
            {"8", _} -> {:halt, "8"}
            {"9", _} -> {:halt, "9"}
            _ -> {:cont, y}
          end
        end)

      first <> last
    end)
    |> Enum.map(&String.to_integer/1)
    |> Enum.sum()
  end
end

Day01.call()
